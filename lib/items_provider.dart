
import 'package:api_makeup/item.dart';
import 'package:api_makeup/items_repository.dart';
import 'package:flutter/material.dart';

class ItemsProvider with ChangeNotifier {
  ItemsProvider._privateConstructor();

  static final ItemsProvider _instance = ItemsProvider._privateConstructor();

  static ItemsProvider get instance => _instance;

  Future<List<Item>> getItems(String query) async {
    List<Item> networkItems = await ItemRepository().getItems(query);
    items = networkItems;
     print(items.length);
    print(
      items[0].name,
    );
    return items;
  }

  List<Item> _items = [];

  List<Item> get items {
    return [..._items];
  }

  set items(List<Item> list){
    _items = list;
    notifyListeners();
  }

  void addItems(List<Item> items) {
    _items = items;
    notifyListeners();
  }

  void clearItems(){
    _items.clear();
    notifyListeners();
  }
}

import 'package:api_makeup/item.dart';
import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {
  ListItem(this.item);

  final Item item;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
              height: 75,
              width: 75,
              child: Image.network(
                item.image,
                fit: BoxFit.fill,
              )),
          Row(
            children: [
              Container(
                width: 60,
                child: FittedBox(
                    child: Text(item.name.length > 30 ?item.name.substring(0, 15): item.name),),
              ),
              Text(item.price),
            ],
          ),
        ],
      ),
    );
  }
}

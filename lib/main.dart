import 'package:api_makeup/detail_page.dart';
import 'package:api_makeup/items_provider.dart';
import 'package:api_makeup/list_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'item.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ItemsProvider.instance,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  final textController = TextEditingController();

  List<Item> items = [];

  void gotoDetailPage(BuildContext ctx, Item item){

    Navigator.of(ctx).push(MaterialPageRoute(builder: (_){
      return DetailPage(item);
    }
    )
    );
  }

  @override
  Widget build(BuildContext context) {
    var itemData = Provider.of<ItemsProvider>(context);
    final listItems = itemData.items;
    return Scaffold(
      appBar: AppBar(
        title: Text('widget.title'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(

            child: TextField(
              controller: textController,
              decoration: InputDecoration(labelText: 'type your query here'),
            ),
          ),
          FlatButton(
            child: Text('Then tap here'),
            onPressed: () {
              String query = textController.text;
              itemData.getItems(query);
            },
          ),
          listItems.length > 0
              ? Container(
                  color: Colors.pinkAccent,

                  height: 450,
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                    itemCount: listItems.length,
                    itemBuilder: (context, index) {
                      print('bilder.build');
                      return InkWell(
                        child: ListItem(listItems[index]),
                        onTap: () => gotoDetailPage(context,listItems[index]),
                      );
                    },
                  ),
                )
              : Text('nothing to show'),
        ],
      ),
    );
  }
}

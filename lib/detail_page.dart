import 'package:api_makeup/item.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  DetailPage(this.item);

  final Item item;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Image.network(item.image),
            Row(
              children: [
                Text(item.name),
                Text(item.price),
              ],
            ),
            Text(item.brand),
            Text(item.description),
          ],
        ),
      ),
    );
  }
}

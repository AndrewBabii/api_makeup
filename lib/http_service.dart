import 'package:http/http.dart' as http;

class HttpService{

  HttpService(this.query);

  final String query;


  Future<dynamic> getItems() async{
    var response = await http.get('http://makeup-api.herokuapp.com/api/v1/products.json?brand=$query');
    return response.body;
  }
}
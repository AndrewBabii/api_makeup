class Item {
  final String image;
  final String price;
  final String brand;
  final String name;
  final String description;

  Item({this.image, this.price, this.brand, this.name, this.description});

  factory Item.fromJson(Map<String, dynamic> jsonData) {
    return Item(
        image: jsonData['image_link'],
        price: jsonData['price'],
        name: jsonData['name'],
        brand: jsonData['brand'],
        description: jsonData['description']);
  }
}
